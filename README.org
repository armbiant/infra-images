* μ-images

*μ-images* is a collection of high performance build scripts for creating
cloud images using Hashicorp's [[https://www.packer.io][Packer]]. The repo is part of [[https://gitlab.com/e2pi/mu][μ-project]], which
aims to simplify cloud native application development.

** Requirements

  - [[https://please.build/][Please.Build]] build system

** Features

- [[./build/packer-image][packer-image]]: A plugin for easily integrating Packer
  to Please build system.

- [[./build/hc-image][hc-image]]: A plugin for building cloud images that target HashiStack
  platform.

** Usage

First add this plugin to your project. In =plugins/BUILD=
#+begin_example python

 plugin_repo(
  name     = "images",
  owner    = "e2pi/mu/infra",
  plugin   = "images",
  revision = "main")

#+end_example

Then update =.plzconfig= to include the following plugin config,
#+begin_example conf

[Please]
version = >= 16.27.1
pluginrepo = "https://gitlab.com/{owner}/{plugin}/-/archive/{revision}/{plugin}-{revision}.tar.gz"

[plugin "images"]
Target = "//build/plugins:images"

#+end_example

You can then use build rules provided by the plugin,
=packer-image= and =hc-image= to build cloud enabled Linux images.
For complete documentation and examples /see/ [[./build/packer-image][packer-image]] and
[[./build/hc-image][hc-image]].

** License

This project is licensed under the [[./LICENSE][Apache v2.0]]

** Status

The project is in the very early stage. None of the code
in this repository should be used in a production environment as it
can change without notice.
