image_boot_command = [
  "<wait5>",
  "root<enter>",
  "setup-interfaces -a<enter>",
  "service networking restart<enter>",
  "setup-sshd -c openssh<enter>",
  "echo PermitRootLogin yes       >  /etc/ssh/sshd_config<enter>",
  "echo PermitEmptyPasswords yes  >> /etc/ssh/sshd_config<enter>",
  "service sshd restart<enter>",
]

image_shutdown_command = "poweroff"
image_ssh_username     = "root"
image_ssh_password     = null
