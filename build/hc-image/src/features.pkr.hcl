#//////////////////////////////////////////////////
# Feature Toggles
#//////////////////////////////////////////////////

variable vault {
  type = bool
  default = false
  description = "configure VM as vault server"
}

variable consul {
  type = bool
  default = false
  description = "configure VM as consul server"
}

variable nomad {
  type = bool
  default = false
  description = "configure VM as nomad server"
}

variable worker {
  type = bool
  default = false
  description = "configure VM as a generic worker"
}
