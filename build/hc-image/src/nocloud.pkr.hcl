#/////////////////////////////////////////////
# QEMU Settings
#/////////////////////////////////////////////

# cpu
variable qemu_machine_type {
  default = "pc"
}
variable qemu_binary {
  default  = "qemu-system-x86_64"
}
variable qemu_accelerator {
  default  = "kvm"
}

# memory
variable qemu_memory {
  type        = number
  default     = 512
  description = "memory(MB) to allocated during build"
}

# boot
variable qemu_efi {
  default     = ""
  description = "set to 1 to use efi"
}
variable qemu_boot_wait {
  default     = "10s"
  description = "if no accelerator, should set at least 30s"
}
variable qemu_boot_key_interval {
  type     = string
  default  = "10ms"
}

# networking
variable qemu_net_device {
  default = "virtio-net"
}
variable qemu_ssh_timeout {
  default = "2m"
}
variable qemu_disk_interface {
  default =  "virtio"
}

# QEMU CLI args
variable qemu_args {
  default = [ ]
}

# QEMU build spec
source qemu "nocloud" {

  # input
  iso_url          = var.image_url
  iso_checksum     = var.image_checksum
  boot_command     = var.image_boot_command
  shutdown_command = var.image_shutdown_command

  # outpus
  vm_name   = var.output_filename
  format    = var.output_format
  disk_size = var.output_disk_size

  # boot
  boot_key_interval = var.qemu_boot_key_interval
  boot_wait         = var.qemu_boot_wait

  # cpu
  machine_type      = var.qemu_machine_type
  qemu_binary       = var.qemu_binary
  accelerator       = var.qemu_accelerator

  # memory
  memory = var.qemu_memory

  #- disk
  disk_interface    = var.qemu_disk_interface
  disk_image        = var.image_is_disk

  # networking
  net_device   = var.qemu_net_device
  ssh_timeout  = var.qemu_ssh_timeout
  ssh_username = var.image_ssh_username
  ssh_password = var.image_ssh_password

  # misc
  headless = true
  output_directory = "."
  #http_directory    = var.http_directory
  qemuargs = var.qemu_args
}


build {
  name         = "linux"
  sources      = [ "source.qemu.nocloud" ]
  provisioner "file"  {
    source      = "./scripts"
    destination = "/tmp/scripts"
  }
  provisioner "shell" {
    env = {

      # config
      "IMAGE_DIST"     = var.image_dist
      #"IMAGE_EMU"      = var.image_emu
      "IMAGE_CLOUD"    = var.image_cloud
      "IMAGE_IS_DISK"  = convert(var.image_is_disk,string)

      # features
      "CONSUL"         = convert(var.consul,string)
      "NOMAD"          = convert(var.nomad,string)
      "VAULT"          = convert(var.vault , string)
      "WORKER"         = convert(var.worker , string)
    }
    inline = [
      "cd /tmp/scripts" ,
      "./build.sh"
    ]
  }
}
