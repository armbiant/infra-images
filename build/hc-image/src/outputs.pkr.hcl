variable output_filename {
  type = string
  description = "output image filename"
}

variable output_format {
  type = string
  default = "qcow2"
  description = "output image format"
}

variable output_disk_size {
  type = string
  default = "5G"
}
