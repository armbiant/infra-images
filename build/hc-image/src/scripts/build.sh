#!/bin/sh
set -eu
set -o pipefail

#############################################################################
##
##  build.sh
##
##  cloud image build script
##
## * Environment Management Environment Variables
##
##    - IMAGE_DIST:   Linux distribution name
##    - IMAGE_CLOUD:  target cloud provider
##
## * Featuer Toggle Flags
##
##   - CONSUL: configure image as consul server
##   - VAULT : configure image as vault secret server
##   - NOMAD : configure image nomad server
##   - WORKER: configure image as default nomad clinent
##
#####################################################################

#####################################################################
# build settings
#####################################################################
export IMAGE_DIST="${IMAGE_DIST}"
export IMAGE_CLOUD="${IMAGE_CLOUD}"
#export IMAGE_EMU="${IMAGE_EMU}"
export IMAGE_IS_DISK="${IMAGE_IS_DISK}"
export BUILD_DIR="$(readlink -f $(dirname $0))"
# feature flags
export VAULT="${VAULT}"
export CONSUL="${CONSUL}"
export NOMAD="${NOMAD}"
export WORKER="${WORKER}"

#####################################################################
# functions
#####################################################################
configure_feature() {

    local _feature="${1}"
    local _config_sh="${BUILD_DIR}/features/${_feature}/configure.sh"
    [ -f "${_config_sh}" ] || {
        berr "unable to configure feature: $_feature"
        berr "$_feature may not be available for $IMAGE_CLOUD/$IMAGE_DIST"
        return 1
    }

    # execute
    "${_config_sh}"

}

#####################################################################
# main
#####################################################################
. ${BUILD_DIR}/utils/log.sh

# install if source image is ISO
if [ "${IMAGE_IS_DISK}" = "false" ]; then

    readonly install_sh="${BUILD_DIR}/install/${IMAGE_DIST}-${IMAGE_CLOUD}/install.sh"
    [ -f "${install_sh}" ] || {

        berr "install script does not exist: $install_sh"
        exit 1
    }
    $install_sh

else

    # configure dist
    readonly config_sh="${BUILD_DIR}/configure/${IMAGE_DIST}-${IMAGE_CLOUD}/configure.sh"
    [ -f "${config_sh}" ] || {
        berr "configuration script does not exist: $config_sh"
        exit 1
    }
    ${config_sh} configure
    [ "${CONSUL}" = "true" ] && configure_feature consul
    [ "${NOMAD}"  = "true" ] && configure_feature nomad
    [ "${VAULT}"  = "true" ] && configure_feature vault
    [ "${WORKER}" = "true" ] && configure_feature worker
    ${config_sh} clean

fi
