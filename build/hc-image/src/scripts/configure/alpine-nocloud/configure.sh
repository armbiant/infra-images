#! /bin/sh
set -eu
set -o pipefail

##################################################
# utilities
###################################################
config_keymap() {
    binfo "keymap"
    setup-keymap us us
}
config_ntp() {
    binfo "ntp"
    setup-ntp chrony
}
config_timezone() {
    binfo "timezone"
    setup-timezone 'UTC'
}
config_networking() {
    binfo "networking"
    setup-interfaces -a
    rc-update add networking boot
    # DNS
    binfo "dns"
    echo "nameserver 8.8.8.8" >> /etc/resolv.conf
    echo "nameserver 8.8.4.4" >> /etc/resolv.conf
    cat /etc/resolv.conf
    rc-service networking restart
}
config_nopswd() {

    binfo "dissable password"
    sed -i 's/^root:[^:]*:/root:*: /' /etc/shadow

}
config_cloud() {

    binfo "cloud configuration"
    # setup cloud-init
    # https://git.alpinelinux.org/aports/tree/community/cloud-init/README.Alpine
    apk add --no-cache      \
        cloud-init          \
        util-linux-misc     \
        openssh-server-pam  \
        ca-certificates
    # enable PAM
    echo "UsePAM yes" >> /etc/ssh/sshd_config
    service sshd restart
    apk add --no-cache qemu-guest-agent gnupg
    rc-update add qemu-guest-agent
    rc-service qemu-guest-agent start
    # enable cloud init
    [[ -d /etc/cloud ]] || mkdir -p /etc/cloud
    cp ${SCRIPT_DIR}/etc/cloud.cfg /etc/cloud/cloud.cfg
    setup-cloud-init
    #mkdir -p /var/lib/cloud/seed/nocloud
    #touch /var/lib/cloud/seed/nocloud/user-data
    #touch /var/lib/cloud/seed/nocloud/meta-data

    return 0
}
config_sshd() {

    binfo "sshd"
    rm /etc/ssh/ssh_host* || :
    setup-sshd  openssh
    #echo 'PasswordAuthentication no' > /etc/ssh/sshd_config
    rc-service sshd restart
}

config_packages() {

    # apk repositories
    binfo "apk repositories"
    readonly ALPINE_VERSION="v$(cat /etc/alpine-release | grep -o '\d\.\d\d')"
    echo "https://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main"        > /etc/apk/repositories
    echo "https://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/community"  >> /etc/apk/repositories
    echo "@edge https://dl-cdn.alpinelinux.org/alpine/edge/main"              >> /etc/apk/repositories
    cat /etc/apk/repositories

    binfo "upgrade system"
    rm -f /lib/apk/db/lock
    apk update
    apk upgrade

    # default packages
    binfo "packages"
    echo  "default: ${DEFAULT_PACKAGES}"
    echo  "temp: ${TMP_PACKAGES}"
    apk add --no-cache ${DEFAULT_PACKAGES} ${TMP_PACKAGES}


}
config_pubkeys() {

    binfo "public key servers"
    export GNUPGHOME="/tmp/gnupg"
    mkdir "${GNUPGHOME}"
    chmod 700 "${GNUPGHOME}"
    gpg --keyserver pgp.mit.edu \
        --keyserver keys.openpgp.org \
        --keyserver keyserver.ubuntu.com \
        --recv-keys "C874 011F 0AB4 0511 0D02 1055 3436 5D94 72D7 468F"
}


##################################################
# commands
###################################################
dist_clean() {
    binfo "clean config"
    gpgconf --kill dirmngr
    gpgconf --kill gpg-agent
    apk del ${TMP_PACKAGES}
    rm -rf /tmp/*
    rm -rf /var/cache/apk/*
    rm -rf /var/tmp/*
}
dist_configure() {
    bhead "setup alpine ..."
    config_networking
    config_packages
    #config_keymap
    config_ntp
    config_timezone
    #config_nopswd
    config_cloud
    config_sshd
    config_pubkeys
}

##################################################
# main
###################################################
. ${BUILD_DIR}/utils/log.sh
#"sed attr grep curl readline binutils findutils readline"
readonly DEFAULT_PACKAGES="gnupg"
readonly TMP_PACKAGES="bash file"
readonly SCRIPT_DIR="$(readlink -f $(dirname $0))"

case $1 in
    configure)
    	dist_configure
    	;;
    clean)
        dist_clean
        ;;
esac
