#! /usr/bin/env bash
BASH_XTRACEFD=1
set -eu
set -o pipefail

. ${BUILD_DIR}/utils/log.sh

binfo "install: consul"
[[ "${IMAGE_DIST}" = "alpine" ]] && {
    apk --no-cache add consul
    rm  -f  /etc/init/consul.d/consul
    rm  -f  /etc/conf.d/consul
    rm  -rf /var/consul
    rm  -rf /etc/consul
	install -m750 -o root   -g consul -d /etc/consul.d
    exit 0
}

berr "unable to install consul"
berr "unsupported Linux distribution: $IMAGE_DIST"
exit 1
