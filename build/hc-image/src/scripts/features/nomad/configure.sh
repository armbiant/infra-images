#! /usr/bin/env bash
BASH_XTRACEFD=1
set -eu
set -o pipefail

. ${BUILD_DIR}/utils/log.sh
. ${BUILD_DIR}/features/nomad/version.sh
export GNUPGHOME="/tmp/gnupg"

binfo "install: nomad"
[[ "${IMAGE_DIST}" = "alpine" ]] && {
    apk --no-cache add nomad
    rm /etc/nomad.d/server.hcl
    rm /etc/init.d/nomad
    rm /etc/conf.d/nomad
    exit 0
}

berr "unable to install consul"
berr "unsupported distribution: $IMAGE_DIST"
exit 1
