#! /usr/bin/env bash
BASH_XTRACEFD=1
set -eu
set -o pipefail

. ${BUILD_DIR}/utils/log.sh
. ${BUILD_DIR}/features/vault/version.sh
export GNUPGHOME="/tmp/gnupg"

bhead "vault server"
binfo "working directory: /tmp/build/vault"
[[ -d /tmp/build/vault ]] && rm -rf /tmp/build/vault
mkdir -p /tmp/build/vault && pushd /tmp/build/vault

### download
readonly MIRROR_URL="https://releases.hashicorp.com/vault/${VAULT_VERSION}"
readonly BIN_FILE="vault_${VAULT_VERSION}_linux_amd64.zip"
readonly SIG_FILE="vault_${VAULT_VERSION}_SHA256SUMS.72D7468F.sig"
readonly CHECKSUM_FILE="vault_${VAULT_VERSION}_SHA256SUMS"

binfo "download: $MIRROR_URL/$BIN_FILE"
wget -q "${MIRROR_URL}/${BIN_FILE}" && file $BIN_FILE
binfo "download: $MIRROR_URL/$SIG_FILE"
wget -q "${MIRROR_URL}/${SIG_FILE}" && file $SIG_FILE
binfo "download: ${MIRROR_URL}/${CHECKSUM_FILE}"
wget -q "${MIRROR_URL}/${CHECKSUM_FILE}" && file $CHECKSUM_FILE

### verify
binfo "verify download"
gpg --batch --verify ${SIG_FILE} ${CHECKSUM_FILE}
grep ${BIN_FILE} ${CHECKSUM_FILE} | sha256sum -c

### install
binfo "install"

# add vault user/group
addgroup -S vault 2>/dev/null
adduser  -S -D -h /var/vault -s /sbin/nologin -G vault -g vault vault 2>/dev/null
unzip -o "${BIN_FILE}"
install -m750 -o root -g vault -D vault /usr/local/bin/vault
install -m750 -o root -g vault -d /etc/vault.d
vault version
