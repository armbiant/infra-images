#! /usr/bin/env bash
BASH_XTRACEFD=1
set -eu
set -o pipefail
. ${BUILD_DIR}/utils/log.sh
. ${BUILD_DIR}/features/nomad/version.sh
export GNUPGHOME="/tmp/gnupg"

binfo "install: worker"
[[ "${IMAGE_DIST}" = "alpine" ]] && {
    apk --no-cache add docker
    exit 0
}
