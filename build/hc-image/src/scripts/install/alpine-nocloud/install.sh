#! /bin/sh
set -eu
set -o pipefail
##########################################################################
# functions
##########################################################################

_setup_disk() {

    binfo "load disk tools"
    setup-apkrepos -1
    apk update
    apk add e2fsprogs parted

    # isntall
    readonly _disk="/dev/vda"
    binfo "setup disk: ${_disk}"
    ERASE_DISKS=${_disk} setup-disk -v -s 0 -m sys ${_disk}
}
_mount_system()  {
    # mount
    binfo "mount file-system"
    mount -t ext4 /dev/vda2 /mnt
    [ -d /mnt/boot ] || mkdir  /mnt/boot
    mount -t ext4 /dev/vda1 /mnt/boot
    ls -l /mnt
}
_umount_system() {
    #umount
    binfo "unmount file system ..."
    umount -v /mnt/boot
    umount -v /mnt

}
_enable_dns() {

    binfo "enable local DNS resolve"
    echo "nameserver 8.8.8.8" >> /mnt/etc/resolv.conf
    echo "nameserver 8.8.4.4" >> /mnt/etc/resolv.conf
    cat /mnt/etc/resolv.conf
}
_enable_ssh() {

    binfo "configure ssh ..."
    [ -d /mnt/etc/sshd ] || mkdir /mnt/etc/sshd
    #[ -f /mnt/etc/ssh/ssh_config ] && mv /mnt/etc/ssh/sshd_config /mnt/etc/ssh/sshd_config.backup
    echo "PermitRootLogin yes"        > /mnt/etc/sshd/sshd_config
    echo "PermitEmptyPasswords yes"  >> /mnt/etc/sshd/sshd_config
    cat /mnt/etc/sshd/sshd_config
}
_disable_tmpfs() {
    # this is to avoid out of disk space errors
    # when using /tmp directory on vm instances with
    # small memory
    binfo "disable mounting /tmp as tmpfs"
    sed -i '/tmpfs/d' /mnt/etc/fstab
    cat /mnt/etc/fstab
}
_clean() {
    # clean
    binfo "clean ..."
    rm -vrf /mnt/tmp/* || :
    rm -vrf /mnt/var/cache/apk/* || :
    rm -vrf /mnt/var/tmp/* || :
}

##########################################################################
# main
##########################################################################
. ${BUILD_DIR}/utils/log.sh

binfo "install Alpine Linux"

_setup_disk
_mount_system
_enable_dns
_enable_ssh
_clean
_disable_tmpfs
_umount_system
