#############################################################################
##
##  log-colors.sh
##
##  Console color management
##
#############################################################################

has_color() {

    # check if CI environment
    [[ -z  ${CI:+x}  ]] || return 1
    # check tput command
    command -v tput   2>&1 > /dev/null || return 1

    return 0
}


if has_color; then

    export COLOR_NONE="$(tput sgr0)"                 # default
    export COLOR_BOLD="$(tput bold)"                 # bold
    export COLOR_INFO="$(tput  setaf 3)"             # yellow
    export COLOR_HINT="$(tput setaf 7)"              # bright white
    export COLOR_WARN="${COLOR_BOLD}${COLOR_INFO}"   # dark yellow
    export COLOR_ERROR="$(tput setaf 1 sqr)"         # red

else

    export COLOR_NONE=""
    export COLOR_BOLD=""
    export COLOR_INFO=""
    export COLOR_HINT=""
    export COLOR_WARN=""
    export COLOR_ERROR=""

fi
