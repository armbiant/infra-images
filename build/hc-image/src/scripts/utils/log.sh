#############################################################################
##
##  log.sh
##
##  build log utility
##
#############################################################################

set -eu
set -o pipefail

. ${BUILD_DIR}/utils/log-colors.sh


# log level"
: ${LOG_LEVEL:=info}
case ${LOG_LEVEL} in
    info)
        readonly stdout="${LOG_FILE:-/dev/stdout}"
        readonly stderr="${LOG_FILE:-/dev/stderr}"
        readonly stdwarn="${LOG_FILE:-/dev/stderr}"
        ;;
    warn)
        readonly stdout="/dev/null"
        readonly stdwarn="${LOG_FILE:-/dev/stderr}"
        readonly stderr="${LOG_FILE:-/dev/stderr}"
        ;;
    error)
        readonly stdout="/dev/null"
        readonly stdwarn="/dev/null"
        readonly stderr="${LOG_FILE:-/dev/stderr}"
        ;;
    quite)
       readonly stdout="/dev/null"
       readonly stdwarn="/dev/null"
       readonly stderr="/dev/null"
       ;;
    *)
        echo "[ERROR]: unknown log level: LOG_LEVEL=${LOG_LEVEL}"
        exit 1
	;;
esac

bhead() {

    printf "\n${COLOR_INFO}==> %s${COLOR_NONE}\n" "${1:-}" > ${stdout}
}

binfo() {

    printf "\n${COLOR_INFO}[INFO]: %s${COLOR_NONE}\n" "${1:-}" > ${stdout}
}

bhint() {
    printf "\n${COLOR_HINT}${COLOR_BOLD}[HINT]: %s${COLOR_NONE}\n" > ${stdout}
}

berr() {
    printf "\n${COLOR_ERROR}[ERROR]: %s${COLOR_NONE}\n" "${1:- }"  > ${stderr}
}

bwarn() {
    printf "\n${COLOR_WARN}[WARN]: %s${COLOR_NONE}\n" "${1:- }" > ${stdwarn}
}


bdie() {
    berr "${1:-}"
    return 1
}
