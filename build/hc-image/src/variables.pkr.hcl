variable image_url {
  type = string
  description = "URL of the source image"
}
variable image_is_disk {
  type = bool
  description = "true image is readonly otherwise false"
}
variable image_checksum {
  type = string
  description = "checksum of the image"
  default     = "none"
}
variable image_boot_command {
  type        = list(string)
  description = "boot command sequence(using Packer syntax)"
}
variable image_shutdown_command {
  type = string
  description = "power off command(packer syntax)"
}
variable image_ssh_username  {
  type = string
  description = "SSH login username"
}
variable image_ssh_password  {
  type = string
  description = "SSH login password"
}
#variable image_emu {
#  type = string
#  default = "qemu"
#  description = "target virtualization environment: qemu"
#}
variable image_cloud {
  type = string
  default = "nocloud"
  description = "cloud provider name"
}
variable image_dist {
  type = string
  description = "the type of Linux distribution"
}
#variable http_directory {
#  type    = string
#  description = ""
#  default = "."
#}
