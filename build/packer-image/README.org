* Packer Integration Module

This module provides =Packer= integration with =Please.Build=.

Features include,

- *Packer Binary Management*: Automate download and management of Packer
  binary. Binary version can be configured using =.plzconfig= file

- *Isolated Packer Configuration*: Isolated Packer configuration per build
  with the option to explicitly share configuration across build targets
  for improved build predictability.

- *Extensibility*: The module can be used as a bases for writing more specialized
  packer modules.

** Build Functions

The module implements =packer_init= and =packer_build= functions. The former
is used to setup Packer configuration directory which contains plugins
and cached artifacts. The latter is used to build an image.


*packer_init*: Initializes a Packer configuration directory and
perform =packer init=. The output of this function, a build rule, can be used as input to =packer_image=.
#+begin_example python

packer_init( name:str, tag:str=None, out:str,srcs:list, vars:dict=None,
   chdir:str=None, deps:list=[ ], visibility:list=None):

#+end_example
|------------+---------------------------------------------------------------------------------|
| name       | Name of the build rule                                                          |
| tag        | Tag to apply to build rule Name                                                 |
| out        | Output filename of the generated image                                          |
| *srcs*       | List of packer template files                                                   |
| *vars*       | Dictionary of Packer variable definitions                                       |
| *chdir*      | The directory to switch to before running Packer build                          |
| *config_dir* | Packer config build rule, which can be ignored or set using `packer_init` rule. |
| deps       | List of rules this Packer build process depends on                              |
| visibility | Visibility of this rule                                                         |

*packer_image* This function returns a build rule that runs =packer build=
to generate images.
#+begin_example python

packer_image( name:str, tag:str=None, out:str,srcs:list,
   vars:dict=None, chdir:str=None, deps:list=[ ], visibility:list=None):

#+end_example
| Argument   | Description                                            |
|------------+--------------------------------------------------------|
| name       | Name of the build rule                                 |
| tag        | Tag to apply to build rule Name                        |
| out        | Output filename of the generated image                 |
| *srcs*       | List of packer template files                          |
| *vars*       | Dictionary of Packer variable definitions              |
| *chdir*      | The directory to switch to before running Packer build |
| *config_dir* | Packer config directory build rule                     |
|            | This can also be set using `packer_init` rule.         |
| deps       | List of rules this Packer build process depends on     |
| visibility | Visibility of this rule                                |

** Configuration

To import this module update =.plzconfig= to contain =images= plugin section,
 #+begin_example conf
[please]
version    = >= 16.26.1

 [plugini "images"]
 Target  = "//build/plugins:images"

 ;; optional
 packer-version = "1.8.5"

 #+end_example

Add the snippet below to =build/plugins/BUILD=
#+begin_example conf
;; File: build/plugins/BUILD
plugin_repo(
  name     = "images",
  plugin   = "images",
  owner    = "e2pi/mu/infra",
  revision = "main")

#+end_example
** Example: Building a Docker Image

This example is taken from [[https://developer.hashicorp.com/packer/tutorials/docker-get-started/docker-get-started-build-image][developers.hashicorp.com]].
The example builds a docker container from Packer template files.
To run the example you need a Linux system with [[https://www.docker.com][Docker]] and
[[https://please.build][Please.Build]] installed.


Create a =git= repository and initialize =please.build=
#+begin_example sh

mkdir packer_tutorial && cd packer_tutorial
git init
plz init

#+end_example

Import the =infra= repo as a plugin as described in the
[[*Configuration][Configuration]] section.

Add Packer template files starting with =required.pkr.hcl= which contains
=packer/equired_plugins= block. Isolating this section in its own file
helps to avoid re-running =packer init= whenever a template file changes.
#+begin_example hcl :tangle ./examples/packer-tutorial/required.pkr.hcl :mkdirp yes
# file: requirements.pkr.hcl
packer {
  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source = "github.com/hashicorp/docker"
    }
  }
}
#+end_example

Add the main Packer template file =docker-ubuntu.pkr.hcl=
#+begin_src hcl :tangle ./examples/packer-tutorial/docker-ubuntu.pkr.hcl :mkdirp yes

# file
source "docker" "ubuntu" {
  image  = "ubuntu:xenial"
  export_path = "docker-ubuntu.tar"
}

build {
  name    = "ubuntu-container"
  sources = [
    "source.docker.ubuntu"
  ]
}

#+end_src

Add a build script =BUILD=
#+begin_src python :tangle ./examples/packer-tutorial/BUILD :mkdirp yes
subinclude("@images//build/packer-image:public")
packer_image(
 name = "docker-ubuntu",
 out  = "docker-ubuntu.tar",
 srcs = [ "docker-ubuntu.pkr.hcl" ])

#+end_src

Run the build
#+begin_example sh

plz build

#+end_example

The output should be available at =plz-out/gen/docker-ubuntu.tar=.
The contents of the tar file can be inspected using,
#+begin_example sh

tar -tvf  plz-out/gen/docker-ubuntu.tar  | less

#+end_example
** Limitation

Currently Packer CLI does not support setting the filename
of the output image. The output filename has to be manually
set in the BUILD script as well as in Packer template file.
